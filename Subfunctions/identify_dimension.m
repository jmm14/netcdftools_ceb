function [dims,newCell,actualCoord]=identify_dimension(cellData,featureType)
% Required inputs:
%	cellData: as output from convert_struc_cell_netcdf
%	featureType:  cell array with names of desired coordinates/dimensions for the supplied fielddata structure. Vector data (timeSeries) are referenced to time
%           can accept the old char that queries get_coordinate_type
%           (backward compbability)
% Outputs: 
% 	Returns dims.names that will be used to define coordintates of netcdf file
%		dims.data: is the length of the given dimension (e.g., TIME or LATITUDE)
%		dims.Atts: structire of attributes for writing netcdf file
%	newCell: cellData which are measured variables like Temperature.
%   actualCoord: actual coordinates found within the dataset (amongst those
%       possible with featureType)

 nC=length(cellData);

 for kk=1:nC
	short_names{kk}=cellData{kk}.short_name;
	%nDims(kk)=cellData{kk}.dimensions; % 0 (1 value), 1 (vector), 2 (matrix)
	mL=length(cellData{kk}.data);
 end
 
 if iscell(featureType)
     coordinates=featureType;
 else
    [coordinates] = get_coordinate_type(featureType);
 end
cc=0;
for kk=1:length(coordinates)
	defD=find(strcmp(coordinates{kk},short_names));
	if ~isempty(defD) % so there's a dim variable in cellData 
		cc=cc+1;
		dims{cc}=cellData{defD};
        actualCoord{cc}=coordinates{kk};
		indToRem(cc)=defD;
    end
    
end 

% Remove dim variables from cellData
if cc==0
	dims{1}.standard_name='index';
	dims{1}.data=max(mL); % creating random indices
	dims{1}.Atts.long_name='Index of estimate (no time or geographical reference)';
	dims{1}.Atts.netCDFtype='double';
	newCell=cellData;
else
	ind=setdiff([1:1:nC],indToRem);
	for kk=1:length(ind) % may not need loop
		%TmpDat=cellData{ind(kk)};
		newCell{kk}=cellData{ind(kk)}; % there's a dimensions field now 
	end
end

 
end % end of main


