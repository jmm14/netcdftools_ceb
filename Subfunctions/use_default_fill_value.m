function defaultFill=use_default_fill_value(data)

%  possibleInputs = {'int8','int16','int32',...
%      'single', 'double', 'char'};

matlabType=class(data);    

switch matlabType
    case{'int8','int16'},    defaultFill = -99;
    %case 'int16',   defaultFill = -32768;
    case{'int32','single','double'},    defaultFill = -99999;
    case 'char',    defaultFill = '-';
    otherwise
        defaultFill=[];
end
end
