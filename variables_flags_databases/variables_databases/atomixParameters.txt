% Copied and somewhat re-organised columns from atomix_netcdf.xlsx on teams
%Parameter (short) Name	CF parameter	Standard name	Units of measurement	Dir positive	Reference datum	Data code	fillValue	validMin	validMax	NetcDF type	Long name	
TIME,1,time,Days since 1950-01-01T00:00:00Z,,,,-999999,,,double,
DEPTH,1,depth,m,down,sea surface,Z,-999999,-5,12000,double,
LATITUDE,1,latitude,degrees_north,,WGS84 geographic coordinate system,,-999999,-90,90,double,
LONGITUDE,1,longitude,degrees_east,,WGS84 geographic coordinate system,,-999999,-180,180,double,
TEMP,1,sea_water_temperature,degrees_Celsius,,,T,-999999,-2.5,40,double,sea water temperature in-situ ITS-90 scale
CNDC,1,sea_water_electrical_conductivity,S m-1,,,C,-999999,0,50000,double,
PSAL,1,sea_water_practical_salinity,1,,,S,-999999,0,41,double,
DENS,1,sea_water_density,kg m-3,,,D,-999999,,,double,
BVFSQ,1,square_of_brunt_vaisala_frequency_in_sea_water,(rad s-1)2,,,T,-999999,0,10000,double,
CSPD,1,water_speed,m s-1,,,V,-999999,0,10,double,
PITCH,1,platform_pitch_angle,degree,,,,-999999,-180,180,double,
HEADING,1,platform_yaw_angle,degree,clockwise,true North,,-999999,0,360,double,
HEADING_MAG,1,platform_yaw_angle,degree,clockwise,magnetic North,,-999999,0,360,double,
ROLL,1,platform_roll_angle,degree,,,,-999999,-180,180,double,
EPSI,1,specific_turbulent_kinetic_energy_dissipation_in_sea_water,W kg-1,,,T,-999999,1.00E-12,1.00E+00,double,dissipation rate of turbulent kinetic energy per unit mass in sea water estimated from individual shear probes / beams
EPSI_FINAL,0,specific_turbulent_kinetic_energy_dissipation_in_sea_water,W kg-1,,,,-999999,,,double,final dissipation rate of turbulent kinetic energy per unit mass in sea water (averaged using all accepted shear probes / beams / or best estimate from single channel)
K_TEMP,0,water_vertical_diffusivity_of_heat,m2 s-1,,,T,-999999,1.00E-12,100,double,
K_RHO,0,water_diapycnal_diffusivity,m2 s-1,,,T,-999999,1.00E-12,100,double,
KAPPA_TEMP,0,seawater_molecular_diffusivity_temperature,m2 s-1,,,P,-999999,1.00E-09,1.00E-05,double,
KVISC,0,kinematic_viscosity_of_water,m2 s-1,,,P,-999999,1.00E-08,1.00E-04,double,Kinematic viscosity of (sea) water
dTEMP_dZ,0,derivative_of_seawater_temperature_wrt_z,degrees_Celsius m-1,,,T,-999999,0,100,double,
PROFILE,0,unique_identifier_for_each_profile_feature_instance_in_this_file,1,,,,-999999,,,int,
SECTION_NUMBER,0,unique_identifier_for_each_section_of_data_from_timeseries,1,,,,-999999,,,int,A unique indentifier counter defining sections of the time series from the fast (or slow) channels extracted for dissipation estimates
PSPD_REL,1,platform_speed_wrt_sea_water,m s-1,,,,-999999,,,double,Platform speed with respect to sea water
SHEAR,0,sea_water_velocity_shear,s-1,,,,-999999,,,double,rate of change of cross axis sea water velocity along transect measured by shear probes
VIB,0,platform_vibration,1,,,,-999999,,,double,platform vibration detected by piezo-accelerometers
ACC,0,platform_acceleration,m s-2,,,,-999999,,,double,platform acceleration detected by accelerometers
SH_SPEC,0,shear_probe_spectrum,s-2 cpm-1,,,,-999999,1.00E-12,1.00E+00,double,cyclic wavenumber spectrum of sea water velocity shear
SH_SPEC_CLEAN,0,shear_probe_spectrum_clean,s-2 cpm-1,,,,-999999,,,double,cleaned cyclic wavenumber spectrum of sea water velocity shear
A_SPEC,0,acceleration_sensor_spectrum,m2 s-4 cpm-1,,,,-999999,1.00E-12,1.00E+00,double,acceleration spectrum from accelerometers
AP_SPEC,0,vibration_sensor_spectrum,1,,,,-999999,1.00E-12,1.00E+12,double,vibration spectrum from vibration sensors
SH_A_SPEC,0,shear_and_acceleration_cross-spectral_matrix,,,,,-999999,,,double,Complex cross-spectral matrix of shear and acceleration
SH_AP_SPEC,0,shear_and_vibration_cross-spectral_matrix,,,,,-999999,,,double,Complex cross-spectral matrix of shear and vibration
KMAX,0,maximum_wavenumber_used_for_estimating_turbulent_kinetic_energy_dissipation,cpm,,,,-999999,0,1000,double,maximum wavenumber for integration of shear spectrum
FOM,0,figure_of_merit,-,,,,-999999,,,double,Figure of merit of shear spectrum relative to the model spectrum. It is MAD multiplied by the sqrt( degrees of freedom) of the spectra
MAD,0,mean_absolute_deviation,-,,,,-999999,,,double,mean absolute deviation of shear spectrum relative to the model spectrum
DOF,0,degrees_of_freedom_of_spectrum,,,,,-999999,0,500,double,
FREQ,0,frequency,Hz,,,,-999999,0,1.00E+04,double,
FREQ_W,0,angular_frequency,rad s-1,,,,-999999,0,1.00E+04,double,
KCYC,0,cyclic_wavenumber,cpm,,,,-999999,0,1.00E+04,double,wavenumber along the direction of instrument motion in cycles per meter
MAD_SQRT_DOF,0,mean_absolute_deviation_multiplied_sqrt_of_degrees_of_freedom,,,,,-999999,0,1000,double,
KMIN,0,minimum_wavenumber_used_for_estimating_turbulent_kinetic_energy_dissipation,cpm,,,,-999999,0,1000,double,minimum wavenumber for integration of shear spectrum
DIR_CSPD,0,direction_of_water_speed,degree,counterclockwise,east,,-999999,0,360,double,
ROT_AXIS,0,axis_of_rotation_from_east_to_x,,,,,-999999,-1,1,double,Rotation axis  in the geographical coordinates system used to rotate from the geographical to the analysis frame of reference
ROT_ANGLE,1,angle_of_rotation_from_east_to_x,degree,counterclockwise,east,,-999999,0,360,double,Rotation angle for rotating data in the geographical coordinates into the analysis frame of reference. To do the opposite, change the sign of ROT_angle
UVW_VEL,0,water_velocity_in_the_analysis_frame_of_reference,ms-1,,analysis frame of reference,V,-999999,-10,10,double,The measurements have been rotated into a frame of reference for which the x (longitudinal) axis coincides with the direction of mean water speed
UVW_VEL_SPEC,0,power_spectrum_density_of_velocity_in_the_analysis_frame_of_reference,(m s-1)2 s,,,V,-999999,1.00E-12,1.00E+00,double,Summation of the spectra across all frequencies is equal to the signal's variance in time-domain.
XYZ_VEL,0,water_velocity_measured_in_instrument_coordinates,ms-1,,,V,-999999,-10,10,double,
BEAM_VEL,0,water_velocity_measured_in_beam_coordinates,ms-1,,,V,-999999,-10,10,double,
ENU_VEL,0,water_velocity_in_geographical_coordinates,ms-1,,Geographical,V,-999999,-10,10,double,
UVW_VEL_DETRENDED,0,water_velocity_in_the_analysis_frame_of_reference_detrended,ms-1,,,V,-999999,-5,5,double,"These velocities in the analysis frame of reference have been detrended. The signal may include surface wave, motion contamination, in addition to the turbulence signal"
UVW_VEL_RMS,0,water_velocity_in_the_analysis_frame_of_reference_standard_deviation_root_mean_square,ms-1,,,V,-999999,0,5,double,An rms calculation on a detrended timeseries (mean removed) is the same as the standard deviation
UVW_VEL_STD,0,water_velocity_in_the_analysis_frame_of_reference_standard_deviation,ms-1,,,V,-999999,0,5,double,
TIME_HPR,0,time_of_heading_pitch_roll_sensors,Days since 1950-01-01T00:00:00Z,,,,-999999,,,double,"Heading, pitch and roll (tilt) sensors measure always at 1Hz for Vector ADV"
TIME_BNDS,0,time_interval_bounds,same as TIME,,,,-999999,,,double,Provides the beginning and end of each interval specified by the variable TIME
TAYL,0,ratio_of_rms_turb_U_to_water_speed,-,,,T,-999999,0,10000,double,
SNR,0,signal_noise_ratio,decibel,,,A,-999999,0,150,double,
ABSIC,0,backscater_intensity,count,,,A,-999999,0,,double,
CORRN,0,noise_correlation_percent,percent,,,E,-999999,0,100,double,
N_VEL_INSTRUMENT,0,unique_identifier_for_each_velocity_instrument,-,,,D,-999999,1,,int,
N_VEL_COMPONENT,0,unique_identifier_for_each_velocity_component,-,,,D,-999999,1,3,int,
N_SAMPLE,0,unique_identifier_for_each_sample_within_the_segment,-,,,D,-999999,1,,int,
N_BOUND,0,unique_identifier_for_defining_low_high_bounds,-,,,T,-999999,1,2,int,"Here 1 represents the lower bound, and 2 the upper bound for errors, or time intervals, etc"
N_SEGMENT,0,unique_identifier_for_each_segment_in_the_entire_available_timeseries,-,,,D,-999999,1,,int,
N_PROFILE,0,unique_identifier_for_each_profile_in_the_entire_available_timeseries,-,,,D,-999999,1,,int,
SPEC_SLOPE,0,estimated_spectral_slope_of_fitted_wavenumbers_in_logspace,-,,,T,-999999,,,double,Nonlinear least square fit to find most likely spectral slope
SPEC_SLOPE_CI_LOW,0,low_limit_confidence_interval_of_spectral_slope_in_logspace,-,,,T,-999999,,,double,
SPEC_SLOPE_CI_HIGH,0,high_limit_confidence_interval_of_spectral_slope_in_logspace,-,,,T,-999999,,,double,
dUCUR_dZ,0,derivative_of_eastward_velocity_wrt_z,s-1,,,V,-999999,-10,10,double,
dVCUR_dZ,0,derivative_of_northtward_velocity_wrt_z,s-1,,,V,-999999,-10,10,double,
dWCUR_dZ,0,derivative_of_upward_velocity_wrt_z,s-1,,,V,-999999,-5,5,double,
HEIGHT_AB,1,height_above_bottom,m,up,bottom,D,-999999,0,12000,double,
BURST_NUMBER,0,unique_identifier_for_each_burst,-,,,D,-999999,1,,int,
PERG,0,percentage_of_samples_good,percent,,,E,-999999,0,100,double,
N_BEAM,0,unique_identifier_for_each_beam,,,,D,-999999,1,10,int,
ABSI,0,backscatter_intensity_from_each_acoustic_beam,decibel,AlongBeam_distance,,,-999999,0,,double,
ABSI5,0,backscatter_intensity_from_acoustic_beam_5,decibel,AlongBeam_distance,,,-999999,0,,double,
CORR,0,correlation_magnitude_from_each_acoustic_beam,counts,AlongBeam_distance,,,-999999,0,,double,
CORR5,0,correlation_magnitude_from_acoustic_beam_5,counts,AlongBeam_distance,,,-999999,0,,double,
R_DIST5,0,distance_from_sensor_along_beam_5,m,outwards from transducer,sensor,,-999999,0,,dimension,Required for 5-beam ADCPs when the 5th is pointing vertically up.
R_DIST,0,distance_from_sensor_along_beams,m,outwards from transducer,sensor,,-999999,0,,dimension,
HEIGHT_AS,1,height_above_sensor,m,up,sensor,Z,-999999,-12000,12000,double,Negative implies below the sensor (downward facing instrument)
THETA,0,beam_angle_from_instrument_z_axis,degrees,clockwise,vertical axis,,-999999,0,90,double,
BIN_SIZE,0,instrument_measurement_volume_bin_size,m,,,E,-999999,0,100,double,
R_VEL,0,water_radial_velocity_of_scatterers_towards_instrument,m/s,towards instrument,,V,-999999,-10,10,double,
R_VEL5,0,water_radial_velocity_of_scatterers_towards_instrument_in_beam_5,m/s,towards instrument,,V,-999999,-5,5,double,
R_VEL_DETRENDED,0,water_radial_velocity_of_scatterers_towards_instrument_detrended,m/s,towards instrument,,V,-999999,-10,10,double,"High-frequency content of the velocity signal, which may contain contributions from surface waves, turbulence, and vibrations etc"
R_VEL5_DETRENDED,0,water_radial_velocity_of_scatterers_towards_instrument_in_beam_5_detrended,m/s,towards instrument,,V,-999999,-10,10,double,"High-frequency content of the velocity signal, which may contain contributions from surface waves, turbulence, and vibrations etc"
R_DEL,0,along-beam_separation_distance_over_which_DLL_is_evaluated,m,,,,-999999,0,,double,
R_DEL5,0,along-beam_separation_distance_over_which_DLL_is_evaluated_in_beam_5,m,,,,-999999,0,,double,
R_MAX,0,maximum_separation_distance_over_for_DLL_regression,m,,,,-999999,0,,double,
DLL5_N,0,second_order_structure_function_in_beam_5,m2/s2,AlongBeam_distance,,,-999999,0,,double,
DLL,0,second_order_structure_function,m2/s2,AlongBeam_distance,,,-999999,0,,double,
DLL_N,0,second_order_structure_function_number_of_observations,count,,,,-999999,0,,int,
DLL5_N,0,second_order_structure_function_in_beam_5_number_of_observations,count,,,,-999999,0,,int,
DLL_FLAGS,0,second_order_structure_function_status_flags,count,,,,-999999,,,double,
C2,0,constant_used_in_the_second_order_structure_function,,,,,-999999,,,double,This constant appears when estimating the dissipation rate of turbulent kinetic energy from the regression coefficients.
REGRESSION_RSQUARED,0,regression_goodness_of_fit_adjusted_for_number_of_terms,,,,,-999999,,,double,
EPSI_CI_LOW,0,specific_turbulent_kinetic_energy_dissipation_in_water_low_confidence_limit,W kg-1,,,,-999999,,,double,95% confidence interval
EPSI_CI_HIGH,0,specific_turbulent_kinetic_energy_dissipation_in_water_high_confidence_limit,W kg-1,,,,-999999,,,double,95% confidence interval
QAx_BDF,0,QA_criterion_x_bad_data_flag,logical,,,,-999999,0,1,logical,
REGRESSION_COEFF_A0,0,structure_function_regression_intercept,m2 s-2,,,,-999999,,,double,
REGRESSION_COEFF_A1,0,structure_function_regression_coefficient_for_r^2/3,m4/3 s-2,,,,-999999,,,double,
REGRESSION_COEFF_A3,0,structure_function_regression_coefficient_for_(r^2/3)^3,s-2,,,,-999999,,,double,
REGRESSION_N,,structure_function_regression_number_of_observations,count,,,,,,,,
L_K,0,kolmogorov_length_scale,m,,,L,-999999,0,1000,double,
L_S,0,turbulent_corssin_shear_length_scale,m,,,L,-999999,0,100000,double,
L_O,0,turbulent_ozmidov_stratification_length_scale,m,,,L,-999999,0,100000,double,